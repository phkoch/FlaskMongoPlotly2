# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 11:03:58 2021

@author: geomet2
"""

import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc

from app import app
# import all pages in the app
from apps import statistics, particles, home, liberation, minerals

dropdown = dbc.DropdownMenu(
    children=[
        dbc.DropdownMenuItem("Home", href="/haha"),
        dbc.DropdownMenuItem("Mineral setup", href="/minerals"),
        dbc.DropdownMenuItem("Statistics", href="/statistics"),
        dbc.DropdownMenuItem("Liberation", href="/liberation"),
        dbc.DropdownMenuItem("Particles", href="/particles"),
        dbc.DropdownMenuItem("Generate Report", href="/particles"),
    ],
    nav = True,
    in_navbar = True,
    label = "Explore",
)

navbar = dbc.Navbar(
    dbc.Container(
        [
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(html.Img(src="/assets/part.png", height="55px")),
                        dbc.Col(dbc.NavbarBrand("AMCO v2 DASH", className="ml-2" )),
                    ],
                    align="center",
                    no_gutters=True,
                ),
                href="/home",
            ),
            dbc.NavbarToggler(id="navbar-toggler2"),
            dbc.Collapse(
                dbc.Nav(
                    # right align dropdown menu with ml-auto className
                    [dropdown], className="ml-auto", navbar=True
                ),
                id="navbar-collapse2",
                navbar=True,
            ),
        ]
    ),
    color="dark",
    dark=True,
)

def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open

for i in [2]:
    app.callback(
        Output(f"navbar-collapse{i}", "is_open"),
        [Input(f"navbar-toggler{i}", "n_clicks")],
        [State(f"navbar-collapse{i}", "is_open")],
    )(toggle_navbar_collapse)

# embedding the navigation bar
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    navbar,
    html.Div(id='page-content')
])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/minerals':
        return minerals.layout
    elif pathname == '/statistics':
        return statistics.layout

# Project meta
# Size
# Mineralogy
# Liberation
# Particles

    elif pathname == '/liberation':
        return liberation.layout
    elif pathname == '/particles':
        return particles.layout
    else:
        return home.layout

if __name__ == '__main__':
    app.run_server(host='127.0.0.1', debug=True,use_reloader=False)
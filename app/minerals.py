# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 11:07:39 2021

@author: geomet2
"""

import dash
from dash.dependencies import Input, Output
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import json


df = pd.read_csv("D:/ph_flask_mongo_plotly/FlaskMongoPlotly/app/hscgeo7_b.csv",sep=";")
# add an id column and set it as the index
# in this case the unique ID is just the country name, so we could have just
# renamed 'country' to 'id' (but given it the display name 'country'), but
# here it's duplicated just to show the more general pattern.
#df['id'] = df['country']
df.set_index('MineralID', inplace=True, drop=False)

layout = html.Div([
    dash_table.DataTable(
        style_cell={
        'width': '{}%'.format(len(df.columns)),
        'textOverflow': 'ellipsis',
        'overflow': 'hidden'
        },
        style_table={'overflowX': 'auto'},
        id='datatable-interactivity',
        columns=[
            {'name': i, 'id': i, 'deletable': True} for i in df.columns
            # omit the id column
            if i != 'id'
        ],
        data=df.to_dict('records'),
        editable=True,
        filter_action="native",
        sort_action="native",
        sort_mode='multi',
        row_selectable='multi',
        row_deletable=True,
        selected_rows=[],
        page_action='native',
        page_current= 0,
        page_size= 10,
    ),
        html.Button('Select', id='submit-val', n_clicks=0),
        html.Div(id='container-button-basic',
             children='Enter a value and press submit')
])

@app.callback(
    Output('container-button-basic', 'children'),
    [
        Input('submit-val', 'n_clicks'),
        Input('datatable-interactivity', "selected_rows"),
    ]
)
def update_graph(
    n_clicks,
    selected_rows,

):

    if selected_rows is None:
        selected_rows = []

    ddf = df.iloc[list(selected_rows)]
    ddf.to_csv('mineralogy.csv',sep=';')
    #up_df = pd.read_csv('file1.csv',sep=';')
# add an id column and set it as the index
# in this case the unique ID is just the country name, so we could have just
# renamed 'country' to 'id' (but given it the display name 'country'), but
# here it's duplicated just to show the more general pattern.
#df['id'] = df['country']
    #up_df.set_index('MineralID', inplace=True, drop=False)
    return [

        html.Div([
    dcc.Textarea(
        id='textarea-example',
        value='Mineralogy saved !',
        style={'width': '100%', 'height': 300},
    ),
    html.Div(id='textarea-example-output', style={'whiteSpace': 'pre-line'})
])
    ]


app.run_server(debug=True)
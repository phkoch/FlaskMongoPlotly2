import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import pymongo  # for working with the MongoDB API
import pandas as pd
import dash_core_components as dcc
import plotly.express as px

# needed only if running this as a single page app
#external_stylesheets = [dbc.themes.LUX]

#app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# MongoDB
print("Connecting to MongoDB...")
CLIENT = pymongo.MongoClient("mongodb://localhost:27017/")
DB = CLIENT["local"]
COLL = DB["analyses"]
TEST = COLL.find_one()
TEST2 = TEST.get("minerals_stats")
PROJ_DB = DB["projects"].find_one()
print(PROJ_DB)
PROJ_NAME = PROJ_DB
TEST3 = pd.DataFrame(TEST.get("area_percentages"))
print("MongoDB data loaded")
# Feed = test['HSCFeedGeo']
# Stream = Feed['HSC8Stream']
# MineralsList = Stream['MineralsList']['Mineral']

# DataFrame t2
DATA0 = pd.DataFrame(TEST2)

# change to app.layout if running as single page app instead
layout = html.Div([
    dbc.Container([
                                 html.P('Please select your project'),
                                 html.Div(
                                     className='div-for-dropdown',
                                     children=[
                                         dcc.Dropdown(id='projselector', options=[{"label": i, "value": i} for i in DATA0.columns],
                                                      value='dataset_lock',
                                                      style={'backgroundColor': '#FFFFFF'},
                                                      className='stockselector'
                                                      ),
                                     ],
                                     style={'color': '#FFFFFF'})
                                ]
                             ),
    dbc.Container([
        dbc.Row(
            [
            html.Button('Load entire project',n_clicks = 0)
            ], justify="center", align="center", className="h-50"
            )
                ]),
        dbc.Container([
                                 html.P('Please select your project sample (weight, size fraction)'),
                                 html.Div(
                                     className='div-for-dropdown',
                                     children=[
                                         dcc.Dropdown(id='projselector', options=[{"label": i, "value": i} for i in DATA0.columns],
                                                      value='dataset_lock',
                                                      style={'backgroundColor': '#FFFFFF'},
                                                      className='stockselector'
                                                      ),
                                     ],
                                     style={'color': '#FFFFFF'})
                                ]
                             ),
            dbc.Container([
        dbc.Row(
            [
            html.Button('Load selected project sample',n_clicks = 0)
            ], justify="center", align="center", className="h-50"
            )
                ]),
            dbc.Container([
                                 html.P('Please select your analytical sample'),
                                 html.Div(
                                     className='div-for-dropdown',
                                     children=[
                                         dcc.Dropdown(id='projselector', options=[{"label": i, "value": i} for i in DATA0.columns],
                                                      value='dataset_lock',
                                                      style={'backgroundColor': '#FFFFFF'},
                                                      className='stockselector'
                                                      ),
                                     ],
                                     style={'color': '#FFFFFF'})
                                ]
                             ),
            dbc.Container([
                dbc.Row(
            [
            html.Button('Load selected analytical sample',n_clicks = 0)
            ], justify="center", align="center", className="h-50"
            )
                ]),


    ])

# needed only if running this as a single page app
# if __name__ == '__main__':
#     app.run_server(host='127.0.0.1', debug=True)
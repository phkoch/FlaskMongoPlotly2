import plotly.graph_objects as go
import pandas as pd
import numpy as np

import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import pymongo  # for working with the MongoDB API
import pandas as pd
import dash_core_components as dcc
import plotly.express as px
from app import app


print("Modules loaded")


# MongoDB
print("Connecting to MongoDB...")
# balance le usr:pwd en clair !!
CLIENT = pymongo.MongoClient("mongodb://pickit:28Vx!HypsmGDvjkZ@139.165.65.71:28001")
#CLIENT.Amco_20210223.authenticate('pickit', '28Vx!HypsmGDvjkZ',source="Amco_20210223")

DB = CLIENT["Amco_20210223"]
COLL = DB["regions"]
PART_COLL = DB["regions"]
TEST = COLL.find({'AREA': { '$gt': 80.0 }, 'MC_PERIMETER':{'$lt':800.0}, 'FERET_ELONGATION':{'$lt':0.58} },
                 {'_id': 1, 'contour_points': 1,
                 'roi_xtl': 1, 'roi_ytl': 1, 'AREA':1,'FERET_ELONGATION':1,'MC_PERIMETER':1})



LIST_PART=list(TEST)
x_glob = []
y_glob = []
loop_index = 0
for e in LIST_PART:
    # all the list with a step of 2
    x_part = e['contour_points'][0::2]
    x_part.append(x_part[0])
    x_part.append(None)
    # all the list with a step of 2 starting with shift 1
    y_part =  e['contour_points'][1::2]
    y_part.append(y_part[0])
    y_part.append(None)
    if loop_index==0:
        fig = go.Figure(go.Scattergl(x=x_part, y=y_part, mode="lines",text='area : '+str(e['AREA'])+'\n'+'elongation : '+str(e['FERET_ELONGATION']) ))
    else:
        fig.add_trace(go.Scattergl(x=x_part, y=y_part, mode="lines",text='area : '+str(e['AREA'])+'\n'+'elongation : '+str(e['FERET_ELONGATION']) ))
    loop_index= loop_index +1

layout = html.Div([
    dcc.Graph(id='plot', figure=fig),
    html.Div([
           html.H3('Filter by minimum area [px]'),
           dcc.Slider(
               id='slider-area',
               min=0,
               max=1000,
               step=1,
               value=80,
               marks={i: str(i) for i in range(0, 1000, 50)}
           ),
           html.H3('Filter by maximum perimeter [px]'),
           dcc.Slider(
               id='slider-perimeter',
               min=0,
               max=1600,
               step=50,
               value=800,
               marks={i: str(i) for i in range(0, 1600, 100)}),
           html.H3('Filter by maximum elongation'),
           dcc.Slider(
               id='slider-elongation',
               min=0,
               max=100,
               step=1,
               value=58,
               marks={i: str(i) for i in range(0, 100, 5)}),
       ])
])
fig.update_layout(
    width = 1000,
    height = 1000,
    title = "Particles view"
)

fig.update_yaxes(
    scaleanchor = "x",
    scaleratio = 1,
  )
@app.callback(Output('plot', 'figure'),
             [Input('slider-area', 'value'),
              Input('slider-perimeter', 'value'),
              Input('slider-elongation', 'value')])

def update_figure(slider_area,slider_peri,slider_elong):
    TEST = COLL.find({'AREA': { '$gt': slider_area }, 'MC_PERIMETER':{'$lt':slider_peri}, 'FERET_ELONGATION':{'$lt':0.01*slider_elong} },
                     {'_id': 1, 'contour_points': 1,
                     'roi_xtl': 1, 'roi_ytl': 1, 'AREA':1,'FERET_ELONGATION':1,'MC_PERIMETER':1})


    LIST_PART=list(TEST)
    x_glob = []
    y_glob = []
    loop_index = 0
    for e in LIST_PART:
        # all the list with a step of 2
        x_part = e['contour_points'][0::2]
        x_part.append(x_part[0])
        x_part.append(None)
        # all the list with a step of 2 starting with shift 1
        y_part =  e['contour_points'][1::2]
        y_part.append(y_part[0])
        y_part.append(None)
        if loop_index==0:
            fig = go.Figure(go.Scattergl(x=x_part, y=y_part, fill='toself', mode="lines",text='area : '+str(e['AREA'])+'\n'+'elongation : '+str(e['FERET_ELONGATION']) ))
        else:
            fig.add_trace(go.Scattergl(x=x_part, y=y_part, fill='toself', mode="lines",text='area : '+str(e['AREA'])+'\n'+'elongation : '+str(e['FERET_ELONGATION']) ))
        loop_index= loop_index +1
        fig.update_layout()
        fig.update_yaxes()
    return fig

fig.update_layout()
fig.update_yaxes()


# @app.callback(
#     Output("graph", "figure"),
#     Input())

# TEST2 = TEST.get("minerals_stats")
# PROJ_ROOT = DB["projects"]
# PROJ_DB = PROJ_ROOT.find_one()
# COLL.find({ '_id': PROJ_DB['analyses'][0 })



# print(PROJ_DB)
# PROJ_NAME = PROJ_DB
# TEST3 = pd.DataFrame(TEST.get("area_percentages"))
# print("MongoDB data loaded")
# # Feed = test['HSCFeedGeo']
# # Stream = Feed['HSC8Stream']
# # MineralsList = Stream['MineralsList']['Mineral']

# # DataFrame t2
# DATA0 = pd.DataFrame(TEST2)
# # Data in lists
# LIBERATION_AREA = DATA0["liberation_area_value"]
# # liberation_peri = data0["liberation_perimeter_value"]
# AREA_PERCENTILE = DATA0["area_percentiles_value"]
# PERIMETER_PERCENTILE = DATA0["perimeter_percentiles_value"]
# CONVEXITY_PERCENTILE = DATA0["convexity_percentiles_value"]
# ELONGATION_PERCENTILE = DATA0["elongation_percentiles_value"]
# SIEVE_PERCENTILE = DATA0["sieve_percentiles_value"]
# AREA_PERCENTAGE = TEST3.value

# EXTERNAL_STYLESHEETS = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

# needed only if running this as a single page app
# if __name__ == '__main__':
#     app.run_server(host='127.0.0.1', debug=True)
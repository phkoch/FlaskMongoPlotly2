# -*- coding: utf-8 -*-
"""
Created on Sat Mar 13 10:04:41 2021

@author: geomet2
"""

import dash
import plotly.graph_objects as go
from dash.dependencies import Input, Output
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import json
from sklearn import cluster, datasets, mixture
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler, OrdinalEncoder, MinMaxScaler
from itertools import cycle, islice
import tensorflow as tf
import tensorflow.keras.layers as lay
import tensorflow_addons as tfa
from tensorflow.keras.layers.experimental import RandomFourierFeatures
from umap.parametric_umap import ParametricUMAP
import matplotlib.pyplot as plt


app = dash.Dash()

df = pd.read_csv("D:/ph_flask_mongo_plotly/FlaskMongoPlotly/app/hscgeo7_b.csv",sep=";")
# add an id column and set it as the index
# in this case the unique ID is just the country name, so we could have just
# renamed 'country' to 'id' (but given it the display name 'country'), but
# here it's duplicated just to show the more general pattern.
#df['id'] = df['country']


df = pd.read_csv("D:/ph_flask_mongo_plotly/FlaskMongoPlotly/app/hscgeo7_b.csv",sep=";")
df = df.fillna(0)
df['MinSymbol'] = df['MinSymbol'].astype('str')
ord_enc = OrdinalEncoder()
df["MinCode"] = ord_enc.fit_transform(df[["MinSymbol"]])

df2 = df.loc[:,'Si %':'Ge %']

df2['Ni %'] = df['Ni %'].replace(['**'],'0.0')
df2 = df2.astype('float32')
df2 = df2.fillna(0)
df2 = df2.loc[:, (df2 != 0).any(axis=0)]
df2 = df2.div(100.0)
# reducer = umap.UMAP(n_components=2,min_dist=0.0001,n_neighbors=5)
df2 = MinMaxScaler().fit_transform(df2)
# embedding = reducer.fit_transform(scaled_penguin_data, y=df['MinCode'])
# mapper = reducer.fit(scaled_penguin_data)
# embedding.shape
# umap.plot.points(mapper, labels=df['MinCode'])
dims = (78,)
n_components = 2

encoder = tf.keras.Sequential([
  tf.keras.Input(shape=dims),
  RandomFourierFeatures(
      output_dim=64,
      scale=4.,
      kernel_initializer='gaussian'),
  lay.Dense(32,activation = lay.PReLU()),
  lay.Dense(16,activation = lay.PReLU()),
  lay.Dense(8,activation = lay.PReLU()),
  lay.Dense(4,activation = lay.PReLU()),
  lay.Dense(units=n_components),
])
encoder.summary()

decoder = tf.keras.Sequential([
    lay.InputLayer(input_shape=(n_components)),
    lay.Dense(4,activation = lay.PReLU()),
    lay.Dense(8,activation = lay.PReLU()),
    lay.Dense(16,activation = lay.PReLU()),
    lay.Dense(32,activation = lay.PReLU()),
    lay.Dense(64,activation = lay.PReLU()),
    lay.Dense(78),])

embedder = ParametricUMAP(
    encoder=encoder,
    decoder=decoder,
    dims=dims,
    n_training_epochs = 10,
    parametric_reconstruction= True,
    autoencoder_loss = True,
    verbose=True,
    optimizer = tf.keras.optimizers.Adadelta(),
    global_correlation_loss_weight = 0.5
    
)
embedding = embedder.fit_transform(df2)

print(embedder._history)
fig, ax = plt.subplots()
ax.plot(embedder._history['loss'])
ax.set_ylabel('Cross Entropy')
ax.set_xlabel('Epoch')


    

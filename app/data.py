# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 16:49:41 2021

@author: geomet2
"""
import pymongo  # for working with the MongoDB API
import pandas as pd

print("All modules loaded")
print("Connecting to MongoDB...")
CLIENT = pymongo.MongoClient("mongodb://localhost:27017/")
MYDB = CLIENT["local"]
MYCOL = MYDB["analyses"]
TEST = MYCOL.find_one()
TEST2 = TEST.get("minerals_stats")
TEST3 = pd.DataFrame(TEST.get("area_percentages"))
print("MongoDB data loaded")
# HSC Specific keys
# Feed = test['HSCFeedGeo']
# Stream = Feed['HSC8Stream']
# MineralsList = Stream['MineralsList']['Mineral']

DATA0 = pd.DataFrame(TEST2)
LIBERATION_AREA = DATA0["liberation_area_value"]
# liberation_peri = data0["liberation_perimeter_value"]
# pas de valeurs voir avec Godefroid
AREA_PERCENTILE = DATA0["area_percentiles_value"]
PERIMETER_PERCENTILE = DATA0["perimeter_percentiles_value"]
CONVEXITY_PERCENTILE = DATA0["convexity_percentiles_value"]
ELONGATION_PERCENTILE = DATA0["elongation_percentiles_value"]
SIEVE_PERCENTILE = DATA0["sieve_percentiles_value"]
AREA_PERCENTAGE = TEST3.value


DF = [
    [
        "Liberation Mineral 0",
        "Liberation Mineral 1",
        "Liberation Mineral 2",
        "Liberation Mineral 3",
        "Liberation Mineral 4",
        "Liberation Mineral 5",
        "Perimeter Mineral 0",
        "Perimeter Mineral 1",
        "Perimeter Mineral 2",
        "Perimeter Mineral 3",
        "Perimeter Mineral 4",
        "Perimeter Mineral 5",
    ],
    LIBERATION_AREA[0][:],
    LIBERATION_AREA[1][:],
    LIBERATION_AREA[2][:],
    LIBERATION_AREA[3][:],
    LIBERATION_AREA[4][:],
    LIBERATION_AREA[5][:],
    PERIMETER_PERCENTILE[0][:],
    PERIMETER_PERCENTILE[1][:],
    PERIMETER_PERCENTILE[2][:],
    PERIMETER_PERCENTILE[3][:],
    PERIMETER_PERCENTILE[4][:],
    PERIMETER_PERCENTILE[5][:],
]

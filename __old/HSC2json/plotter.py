# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 16:49:41 2021

@author: geomet2
"""
import os # to create an interface with our operating system
import sys # information on how our code is interacting with the host systemimp
import pymongo # for working with the MongoDB API
import pandas as pd
import chart_studio as ct
import chart_studio.plotly as py
import plotly.graph_objs as go
import plotly
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import cufflinks as cf
print('ok loaded')

client = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = client["Outotec"]
mycol = mydb["ph0"]

test = mycol.find_one()
Feed = test['HSCFeedGeo']
Stream = Feed['HSC8Stream']
MineralsList = Stream['MineralsList']['Mineral']

data = pd.DataFrame(list(MineralsList))

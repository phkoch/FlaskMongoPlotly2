import os # to create an interface with our operating system
import sys # information on how our code is interacting with the host systemimp
import pymongo # for working with the MongoDB API
import pandas as pd
import xml.etree.ElementTree as ET
import json
from bson import json_util



client = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = client["Outotec"]
mycol = mydb["ph0"]

import chart_studio.plotly as pty
from plotly.graph_objs import *
import plotly.express as px
#file = os.open(r'D:\new.json',666)
with open(r"D:\pyAMCO\data.json") as jsonfile:
    data = json.load(jsonfile)
    x = mycol.insert_one(data)



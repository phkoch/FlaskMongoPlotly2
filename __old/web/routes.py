# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 15:34:43 2021

@author: geomet2
"""

"""Routes for parent Flask app."""

import pandas as pd
import numpy as np
from flask import render_template, url_for, flash, redirect, request, make_response, jsonify, abort
from web.utils import utils,plotly_plot
from flask import current_app as app
from flask import render_template



@app.route("/")
def home():
    """Landing page."""
    return render_template(
        "index.jinja2",
        title="Plotly Dash Flask Tutorial",
        description="Embed Plotly Dash into your Flask applications.",
        template="home-template",
        body="This is a homepage served with Flask.",
    )
#loading data
#total_confirmed, total_death, total_recovered, df_pop = utils.load_data()


@app.route("/plotly")
def plot_plotly_global():
    final_df = utils.load_data()
    #print(final_df)
    # total confirmed cases globally
    # total_all_confirmed = total_confirmed[total_confirmed.columns[-1]].sum()
    # total_all_recovered = total_recovered[total_recovered.columns[-1]].sum()
    # total_all_deaths = total_death[total_death.columns[-1]].sum()
    #plotting
    ID = final_df[0:3]
    Mineral = final_df[0:3]
    xy = plotly_plot.plotly_simple_xy(final_df)
    context = {'plotly_simple_xy':xy}
    return render_template('plotly.html', context=context)

# Global cases pages
@app.route("/bar")
def globe_map_copy():
    final_df = utils.load_data()
    bar = plotly_plot.plotly_bar()
    context = {'plotly_bar':bar}
    return render_template('globe_map_copy.html', context=context)



